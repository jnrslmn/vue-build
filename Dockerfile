FROM nginx:1.14.2-alpine

COPY dist /var/www/html/public
COPY nginx.conf /etc/nginx/nginx.conf
COPY localhost.conf /etc/nginx/conf.d/default.conf
