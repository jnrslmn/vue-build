import NProgress from 'nprogress'
import Axios from 'axios'

const url = 'http://localhost:4000/'

const http = {

  request (method, path, params) {
    NProgress.start()

    return Axios.request({
      url: url + path,
      data: params,
      method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        // insert here http header
      },
      body: JSON.stringify(params)
    })
      .then(response => {
        return response
      })
  },

  post (url, params) {
    return this.request('POST', url, params)
  },

  get (url, params) {
    return this.request('GET', url, params)
  },

  put (url, params) {
    return this.request('PUT', url, params)
  },

  delete (url, params) {
    return this.request('DELETE', url, params)
  },

  init () {
    // intercept here ...
    Axios.interceptors.request.use(function (config) {
      NProgress.start()
      // Do something before request is sent

      return config
    }, function (error) {
      // Do something with request error
      return Promise.reject(error)
    })

    // Add a response interceptor
    Axios.interceptors.response.use(function (response) {
      // Do something with response dat
      NProgress.done()
      return response
    }, function (error) {
      // Do something with response error
      if (error.response.status === 400 || error.response.status === 401) {

      }
      return Promise.reject(error)
    })
  }

}

export default http
