import http from '../services/http'

export default {
  namespaced: true,
  state: {
    todos: [],
    todo: {}
  },

  getters: {
    getTodos: state => state.todos,
    getTodosById: (state) => (id) => {
      return state.todos.find(item => item.id === id)
    },
    getNotDoneTodos: state => state.todos.filter(item => item.done === false),
    doneTodos: state => state.todos.filter(item => item.done === true),
    doneTodosLength: (state, getters) => {
      return getters.doneTodos.length
    }
  },

  mutations: {
    addTodo (state, payload) {
      state.todos.push(payload)
    },
    receivedTodo (state, payload) {
      console.log(payload)
      state.todos = payload
    },
    setCurrentTodo (state, payload) {
      state.todo = payload
    }
  },

  actions: {
    fetchApi ({ state, commit }) {
      http.get('todos')
        .then(response => {
          console.log(response)
          commit('receivedTodo', response.data)
        })
        .catch(error => {
          console.log(error)
        })
    },

    postTodo ({ state, commit }, payload) {
      http.post('todos', payload)
        .then(response => {
          console.log(response)
          commit('addTodo', response.data)
        })
        .catch(error => {
          console.log(error)
        })
      // fetch('http://localhost:4000/todos', {
      //   method: 'POST',
      //   headers: {
      //     'Accept': 'application/json',
      //     'Content-Type': 'application/json'
      //   },
      //   body: JSON.stringify(payload)
      // })
      //   .then(response => response.json())
      //   .then(response => {
      //   })
    }
  }
}
